package calculator;

public class WrongExpressionException extends Exception {
    private static final long serialVersionUID = 1L;

    public WrongExpressionException() {
        super();
    }

    public WrongExpressionException(String message) {
        super(message);
    }
}
