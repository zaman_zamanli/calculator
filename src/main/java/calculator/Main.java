package calculator;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
    private static Calculator calc;
    private static final Logger LOGGER = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {
        if (args.length > 0) {
            // Set up log level first
            if  (args.length > 1) {
                if (args[1].toLowerCase().equals("info")) {
                    LOGGER.setLevel(Level.INFO);
                } else if (args[1].toLowerCase().equals("debug")) {
                    LOGGER.setLevel(Level.WARNING);
                } else {
                    LOGGER.setLevel(Level.SEVERE);
                }
            } else {
                LOGGER.setLevel(Level.OFF);
            }
            LOGGER.info("Log level is set to: " + LOGGER.getLevel().toString());

            calc = new Calculator(LOGGER);

            String userInput = args[0];
            System.out.println("Your expression is: " + userInput);
            LOGGER.info("User's expression is: " + userInput);

            try {
                Integer result = calc.calculateExpression(userInput);
                LOGGER.info("Result is: " + result);
                System.out.println("Result is: " + result);
            } catch (WrongExpressionException e) {
                LOGGER.info("The expression is wrong.");
                System.out.println("The expression is wrong.");
            } catch (ArithmeticException e) {
                LOGGER.info("Can't divide by 0 or negative integer.");
                System.out.println("Can't divide by 0 or negative integer.");
            }
        } else {
            System.out.println("You have to enter the expression via command line e.g. \"add(5, 4)\"");
        }
    }
}
