package calculator;

import java.util.logging.Logger;

public class Calculator {
private Logger LOGGER;

    public Calculator (Logger logger) {
        LOGGER = logger;
    }

    /**
     * Splits the inner expression into 2 parts
     * 
     * @param expr operator of the inner expression
     * @param innerExpr inner expression
     * @return array which contains both parts of the expression
     */
    private String[] getBothParts (String expr, String innerExpr) {
        LOGGER.info("Splitting the expression " + innerExpr + ".");
        String expr1 = "";
        String expr2 = "";
        int brackets = 0;
        boolean isLet;
        if (expr.equals("let")) {
            isLet = true;
        } else {
            isLet = false;
        }
        for (int i = 0; i<innerExpr.length(); i++) {
            if (innerExpr.charAt(i) == '(') {
                brackets++;
            }
            if (innerExpr.charAt(i) == ')') {
                brackets--;
            }
            if (innerExpr.charAt(i) == ',') {
                if (brackets == 0 && !isLet) {
                    expr2 = innerExpr.substring(i+1);
                    break;
                } else {
                    isLet = false;
                    expr1 = expr1 + innerExpr.charAt(i);
                }
                
            } else {
                expr1 = expr1 + innerExpr.charAt(i);
            }
        }
        expr1 = expr1.trim();
        expr2 = expr2.trim();
        LOGGER.info("Left part of the expression is: " + expr1);
        LOGGER.info("Right part of the expression is: " + expr2);
        String[] returnArray = {expr1, expr2};
        return returnArray;
    }

    /**
     * Returns true if the string can be converted to an integer
     * 
     * @param potentialInteger String to check
     * @return true or false
     */
    private boolean isInteger (String potentialInteger) {
        LOGGER.info("Checking if string " + potentialInteger + " is integer or an expression.");
        try {
            Integer.valueOf(potentialInteger);
            LOGGER.info("String " + potentialInteger + " can be converted to an integer.");
            return true;
        } catch (NumberFormatException e) {
            LOGGER.info("String " + potentialInteger + " cannot be converted to an integer.");
            return false;
        }
    }

    /**
     * Replaces variables with value in let expression
     * 
     * @param input String where we want to replace
     * @param variable String to replace
     * @param value String which will replace variable
     * @return new string
     */
    private String replaceLetVariable (String input, String variable, String value) {
        LOGGER.info("Replacing let variables.");
        LOGGER.info("Replacing " + variable + " with " + value + " in " + input + ".");
        char charVariable = variable.charAt(0);
        String newString = "";
        for (int i = 0; i < input.length(); i++) {
            if (input.charAt(i) == charVariable && i>0 && i+1 < input.length()) {
                if (input.charAt(i-1) == '(' || input.charAt(i+1) == ')') {
                    newString = newString + value;
                    LOGGER.info("Replaced " + input.charAt(i) + " with " + value + ".");
                } else {
                    newString = newString + input.charAt(i);
                }
            } else {
                newString = newString + input.charAt(i);
            }
        }
        return newString;
    }

    /**
     * The method calculates the expression
     * 
     * @param input Full expression
     * @return the result of the expression
     * @throws WrongExpressionException
     */
    protected Integer calculateExpression (String input) throws WrongExpressionException {
        String expr;
        LOGGER.info("The expression is: " + input);
        try {
            expr = input.substring(0, 3);
            LOGGER.info("The operation of the expression is: " + expr);
        } catch (StringIndexOutOfBoundsException e) {
            LOGGER.severe("Unable to get operator from the following expression: " + input);
            throw new WrongExpressionException();
        }

        String[] parts = getBothParts(expr, input.substring(input.indexOf('(') + 1, input.length()-1));
        String expr1 = parts[0];
        String expr2 = parts[1];
        boolean isInteger1 = isInteger(expr1);
        boolean isInteger2 = isInteger(expr2);
        LOGGER.info("Operator is: " + expr);
        if (expr.equals("add")) {
            if (isInteger1 && isInteger2) {
                LOGGER.info("Both of them are integers, adding the following integers: " + expr1 + " and " + expr2);
                return Integer.valueOf(expr1) + Integer.valueOf(expr2);
            } else if (isInteger1 && !isInteger2) {
                LOGGER.info("Adding " + expr1 + " and the result of " + expr2);
                return Integer.valueOf(expr1) + calculateExpression(expr2);
            } else if (!isInteger1 && isInteger2) {
                LOGGER.info("Adding the result of " + expr1 + " and " + expr2 );
                return calculateExpression(expr1) + Integer.valueOf(expr2);
            } else if (!isInteger1 && !isInteger2){
                LOGGER.info("Adding results of " + expr1 + " and " + expr2);
                return calculateExpression(expr1) + calculateExpression(expr2);
            } else {
                LOGGER.severe("One of the following expressions is wrong: " + expr1 + " or " + expr2);
                throw new WrongExpressionException();
            }
        } else if (expr.equals("sub")) {
            if (isInteger1 && isInteger2) {
                LOGGER.info("Both of them are integers, subtracting the following integers: " + expr1 + " and " + expr2);
                return Integer.valueOf(expr1) - Integer.valueOf(expr2);
            } else if (isInteger1 && !isInteger2) {
                LOGGER.info("Subtracting " + expr1 + " and the result of " + expr2);
                return Integer.valueOf(expr1) - calculateExpression(expr2);
            } else if (!isInteger1 && isInteger2) {
                LOGGER.info("Subtracting the result of " + expr1 + " and " + expr2 );
                return calculateExpression(expr1) - Integer.valueOf(expr2);
            } else if (!isInteger1 && !isInteger2) {
                LOGGER.info("Subtracting results of " + expr1 + " and " + expr2);
                return calculateExpression(expr1) - calculateExpression(expr2);
            } else {
                LOGGER.severe("One of the following expressions is wrong: " + expr1 + " or " + expr2);
                throw new WrongExpressionException();
            }
        } else if (expr.equals("mul")) {
            if (isInteger1 && isInteger2) {
                LOGGER.info("Both of them are integers, multiplying the following integers: " + expr1 + " and " + expr2);
                return Integer.valueOf(expr1) * Integer.valueOf(expr2);
            } else if (isInteger1 && !isInteger2) {
                LOGGER.info("Multiplying " + expr1 + " and the result of " + expr2);
                return Integer.valueOf(expr1) * calculateExpression(expr2);
            } else if (!isInteger1 && isInteger2) {
                LOGGER.info("Multiplying the result of " + expr1 + " and " + expr2 );
                return calculateExpression(expr1) * Integer.valueOf(expr2);
            } else if (!isInteger1 && !isInteger2){
                LOGGER.info("Multiplying results of " + expr1 + " and " + expr2);
                return calculateExpression(expr1) * calculateExpression(expr2);
            } else {
                LOGGER.severe("One of the following expressions is wrong: " + expr1 + " or " + expr2);
                throw new WrongExpressionException();
            }
        } else if (expr.equals("div")) {
            if (isInteger1 && isInteger2) {
                LOGGER.info("Both of them are integers, divinding the following integers: " + expr1 + " and " + expr2);
                if (Integer.valueOf(expr2) > 0) {
                    LOGGER.info("Dividing " + expr1 + " by " + expr2 + " is ok");
                    return Integer.valueOf(expr1) / Integer.valueOf(expr2);
                } else if (Integer.valueOf(expr2) < 0){
                    LOGGER.warning("Dividing " + expr1 + " by the negative number " + expr2);
                    return Integer.valueOf(expr1) / Integer.valueOf(expr2);
                } else {
                    LOGGER.severe("Cannot divide " + expr1 + " by 0: " + expr2);
                    throw new WrongExpressionException();
                }
            } else if (isInteger1 && !isInteger2) {
                LOGGER.info("Dividing " + expr1 + " and the result of " + expr2);
                return Integer.valueOf(expr1) / calculateExpression(expr2);
            } else if (!isInteger1 && isInteger2) {
                LOGGER.info("Dividing the result of " + expr1 + " and " + expr2 );
                if (Integer.valueOf(expr2) > 0) {
                    LOGGER.info("Dividing " + expr1 + " by " + expr2 + " is ok");
                    return calculateExpression(expr1) / Integer.valueOf(expr2);
                } else if (Integer.valueOf(expr2) < 0){
                    LOGGER.warning("Dividing " + expr1 + " by the negative number " + expr2);
                    return calculateExpression(expr1) / Integer.valueOf(expr2);
                } else {
                    LOGGER.severe("Cannot divide " + expr1 + " by 0: " + expr2);
                    throw new WrongExpressionException();
                }
            } else if (!isInteger1 && !isInteger2){
                LOGGER.info("Dividing results of " + expr1 + " and " + expr2);
                return calculateExpression(expr1) / calculateExpression(expr2);
            } else {
                LOGGER.severe("One of the following expressions is wrong: " + expr1 + " or " + expr2);
                throw new WrongExpressionException();
            }
        } else if (expr.equals("let")) {
            String[] letBothParts = getBothParts("notlet", expr1);
            String letVariable = letBothParts[0];
            String letValue = letBothParts[1];
            if (isInteger(letValue)) {
                LOGGER.info("Calculating the expression with replacing " + letVariable + " with " + letValue + " in " + expr2);
                return calculateExpression(replaceLetVariable(expr2, letVariable, letValue));
            } else {
                LOGGER.info("Calculating the expression with replacing " + letVariable + " with the result of " + letValue + " in " + expr2);
                return calculateExpression(replaceLetVariable(expr2, letVariable, calculateExpression(letValue).toString()));
            }
        } else {
            LOGGER.severe("The following operator is wrong: " + expr);
            throw new WrongExpressionException();
        }
    }
}
