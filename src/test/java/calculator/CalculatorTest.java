package calculator;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Before;
import org.junit.Test;
import junit.framework.Assert;
import junit.framework.TestCase;

public class CalculatorTest extends TestCase {
    private Calculator calc;
    private static Logger LOGGER = Logger.getLogger(CalculatorTest.class.getName());

    @Before
    protected void setUp() throws Exception {
        super.setUp();
        LOGGER.setLevel(Level.OFF);
        calc = new Calculator(LOGGER);
    }

    @Test
    public void testSimpleAdd() {
        String expression = "add(5, 4)";
        int actual;
        try {
            actual = calc.calculateExpression(expression);
            Assert.assertEquals(9, actual);
        } catch (WrongExpressionException e) {
            Assert.fail();
        }
    }

    @Test
    public void testComplicatedExpression() {
        String expression = "let(a, mult(32, add(5, 4)), div(1232, a))";
        int actual;
        try {
            actual = calc.calculateExpression(expression);
            Assert.assertEquals(4, actual);
        } catch (WrongExpressionException e) {
            Assert.fail();
        }
    }

    @Test
    public void testComplicatedExpression2() {
        String expression = "let(a, let(b, 10, add(b, b)), let(b, 20, add(a, b)))";
        int actual;
        try {
            actual = calc.calculateExpression(expression);
            Assert.assertEquals(40, actual);
        } catch (WrongExpressionException e) {
            Assert.fail();
        }
    }

    @Test
    public void testComplicatedExpression3() {
        String expression = "let(a, 5, let(b, mult(a, 10), add(b, a)))";
        int actual;
        try {
            actual = calc.calculateExpression(expression);
            Assert.assertEquals(55, actual);
        } catch (WrongExpressionException e) {
            Assert.fail();
        }
    }

    @Test
    public void testComplicatedExpression4() {
        String expression = "let(a, 5, add(a, a))";
        int actual;
        try {
            actual = calc.calculateExpression(expression);
            Assert.assertEquals(10, actual);
        } catch (WrongExpressionException e) {
            Assert.fail();
        }
    }

    @Test
    public void testComplicatedExpression5() {
        String expression = "let (b, let (a, add ( mult(5, 30), 43), add(a, mult(4, 5))), mult (b, 32))";
        int actual;
        try {
            actual = calc.calculateExpression(expression);
            Assert.assertEquals(6816, actual);
        } catch (WrongExpressionException e) {
            Assert.fail();
        }
    }

    @Test
    public void testWrongExpression() {
        String expression = "let(a, mult(32, add(5, 4)), div(1232, a)";
        try {
            calc.calculateExpression(expression);
            Assert.fail();
        } catch (WrongExpressionException e) {
            Assert.assertTrue(true);
        }
    }

    @Test
    public void testWrongExpression2() {
        String expression = "div(32, 0)";
        try {
            calc.calculateExpression(expression);
            Assert.fail();
        } catch (WrongExpressionException e) {
            Assert.assertTrue(true);
        }
    }

}
