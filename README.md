# README #


### What is this repository for? ###
This calculator program in Java that evaluates expressions in a very simple integer expression language. The program takes an input on the command line, computes the result, and prints it to the console.
In order to set up logging, the second argument should be one of the followings: INFO, ERROR, and DEBUG. To turn off the logging, leave second argument empty.

### How do I get set up? ###
You can import the project as Maven project into Eclipse IDE or build it using command line command "mvn package"
You can run the program using Eclipse IDE or command line command "java -cp target/calculator-1.0.jar calculator.Main"